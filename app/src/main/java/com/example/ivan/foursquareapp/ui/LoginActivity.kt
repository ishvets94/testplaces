package com.example.ivan.foursquareapp.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import com.example.ivan.foursquareapp.R
import com.example.ivan.foursquareapp.utils.getIsLogin
import com.example.ivan.foursquareapp.utils.setIsLogin
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : MvpAppCompatActivity() {


    lateinit private var mCallbackManager: CallbackManager
    lateinit private var loginManager: LoginManager

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, LoginActivity::class.java)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initUi()
        initFb()
        if (getIsLogin()) {
            PlacesActivity.start(this)
            finish()
        }
    }


    private fun initUi() {
        loginButton.setOnClickListener { login() }
    }

    private fun initFb() {
        mCallbackManager = CallbackManager.Factory.create()
        loginManager = LoginManager.getInstance()
        loginManager.registerCallback(mCallbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                setIsLogin(true)
                PlacesActivity.start(this@LoginActivity)
                finish()
            }

            override fun onCancel() {
                toast(getString(R.string.cancel_login))
            }

            override fun onError(error: FacebookException) {
                toast(error.localizedMessage)
            }
        })
    }

    private fun login() {
        loginManager.logInWithReadPermissions(this, listOf("public_profile"))
    }

    private fun toast(message: String, length: Int = Toast.LENGTH_SHORT) {
        Toast.makeText(this, message, length).show()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (data != null) {
            mCallbackManager.onActivityResult(requestCode, resultCode, data)
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

}
